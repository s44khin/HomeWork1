package com.example.homework1

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.homework1.databinding.MainActivityBinding


class MainActivity : AppCompatActivity() {

    private val binding by lazy {
        MainActivityBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        supportFragmentManager.beginTransaction()
            .add(R.id.fragmentContainer1, FirstFragment())
            .commit()

        supportFragmentManager.beginTransaction()
            .add(R.id.fragmentContainer2, SecondFragment())
            .commit()

    }

}
