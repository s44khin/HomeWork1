package com.example.homework1


import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.homework1.databinding.FragmentSecondBinding


class SecondFragment : Fragment() {

    private val viewModel: ShareViewModel by lazy {
        ViewModelProvider(requireActivity())[ShareViewModel::class.java]
    }

    private var _binding: FragmentSecondBinding? = null
    private val binding get() = _binding ?: error("NE PON.")

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSecondBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.data.observe(viewLifecycleOwner) {
            binding.outputTextView.text = it
        }
    }

}